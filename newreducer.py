newinput = open("sorted.txt","r")
newoutput = open("final.txt","w")

# intialize key value pairs
key = ""
value = 0

for line in newinput:
    data = line.strip().split(',')
    if (len(data) == 2):
        category, count = data

        if (category != key):
            if key:
                newoutput.write(key + "," + str(value) + "\n")
            key = category
            value = 0

        value = value + 1

newoutput.write(key + "," + str(value) + "\n")


newinput.close()
newoutput.close()